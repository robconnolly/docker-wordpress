
# Docker Image for Wordpress

Docker image for Wordpress. Based on the [official Wordpress
image](https://hub.docker.com/_/wordpress/) but containing extra system
packages that I require.

I only maintain this image for my own use, use by third parties is not
recommended. Most likely you should be using the upstream image, not this one!
