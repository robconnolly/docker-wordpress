FROM wordpress:apache

RUN apt-get update && apt-get install -y --no-install-recommends graphicsmagick && \
    apt-get clean && rm -rf /var/lib/apt/lists/*
